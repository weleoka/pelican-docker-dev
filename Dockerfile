FROM python:3.7-alpine3.10

# These values are defaults, docker-compose args block takes precedence
ARG PELICAN_USER=pelicandef
ARG HOME_DIR=/home/pelicandef
ARG SITE_DIR=default_site

# Add bash and other tools to Alpine
#RUN apk add --no-cache bash
#RUN apk add --no-cache coreutils grep sed
RUN apk add --no-cache make

# Add custom tools for your particular requirements
#RUN apk add --no-cache jpeg-dev zlib-dev
#RUN apk add --no-cache gcc musl-dev

# Change current working directory
WORKDIR $HOME_DIR

# Run under non-root user (-D adds user without requiring a password).
RUN adduser -D $PELICAN_USER
RUN chown -R $PELICAN_USER:$PELICAN_USER ./

# Set the active user for container
USER $PELICAN_USER

# Copy the required files to image
RUN echo "Currently working with the site source in: ${SITE_DIR}."
COPY ./$SITE_DIR/requirements.txt ./requirements.txt
COPY ./server.sh ./server.sh

# Set up the python virtual environment
RUN python -m venv venv
RUN source ./venv/bin/activate

# Get latest version of pip and install requirements
RUN ./venv/bin/pip install --upgrade pip
RUN ./venv/bin/pip install --ignore-installed -r requirements.txt

# Change the current working directory
WORKDIR $HOME_DIR/$SITE_DIR

# Entrypoint for container. Also suitable to just use docker-compose.yml
#ENTRYPOINT ["/bin/sh", "pwd"] 
#ENTRYPOINT ["./server.sh"]