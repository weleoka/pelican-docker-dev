# Pelican base with venv on docker
This is a base for developing a Pelican static site using docker, aided with docker-compose and python in a virtual environment.

Python 3.7 on Alpine 3.10 and Pelican 4.1

Tested using `Docker 19.03` and `Docker-compose 1.24`

The directory structure is as such that a `homedir` is used for the user in the container. This directory will have files shared between host and container with such things as python virtual environment, bash/ash/sh and python history files. Also the pip3 cache will be found here. Scripts for deployment, server and project management would be placed here too. 

The `sitedir` will become a sub-directory of `homedir`  in the container, which is where your pelican site repository is.


## Using this project
If you don't already have a pelican site source material then the following will get you started.
1. Create a folder for your site in root of repository
2. Add the name for the folder in the `SITE_DIR` environment variable in the `.env` file.
3. make sure that there is a `requirements.txt` file present in your project folder, copy it in from the `default_site` if needed.
4. The following builds the cimage, launches the container and enters it, then enters the virtual python environment and finally runs pelican-quickstart which will set up a new project in the folder specified in the `SITE_DIR`  variable:

```
docker-compose up --build
docker exec -it pelican-dev /bin/sh
source ../venv/bin/activate
pelican-quickstart
```


The `.env` file sets the directory variables. So if your site is in a folder called `my_site` then that is easy to set in this file with the result that docker and the python venv running in the resulting container orchestrated by docker-compose will work from this instead. 

A few things to remember:
- Make sure that your site home directory has a `requirements.txt`.
- If cachanging between sites then `docker-compose build --no-cache` will force building a new image with the new value for `SITE_DIR` environment variable specified in `.env`.


## Alpine linux
The image is based on Alpine linux which means shell and other applications are extremely limited. In the Dockerfile there is a line which will add bash if it's required. Normally just `/bin/sh` will work.


## Docker compose
This project uses docker compose for the simple reason that it is nice to have it already implemented should the base be expanded and other microservices or dependencies on other systems become relevant. 

[Docker compose docs](https://docs.docker.com/compose/)

### Volumes and mounts
as long as the mounted host volume is empty then the container's volume contents will be copied to the host. 

### Environment variables
The projects aims to be easily configurable and this done with environment variables. Docker compose finds defaults for these in the `.env` file. If any sensitive variables have to be set then it is possible to set the import of the variables from a file such as `.env_private`. See the `docker-compose.yml` file for that. Like this it is possilbe to exclude it from the git repository too.

The variables are also passed on to the container, again see `docker-compose.yml`. 

The priority of variables as far as docker-compose is concerned is:
1. Compose file
2. Shell environment variables
3. Environment file
4. Dockerfile
5. Variable is not defined

What is important to remember is that when docker builds the images the the actual environment variables specified in `docker-compose.yml` are not passed in. Instead it is necessary to use `ARG` in the Dockerfile, and also an `args` block in `docker-compose.yml`.

The way to remember it is that ENV variables are available in the containers, whereas ARG is used for the build process only.

### Keeping containers running
Specifying `tty: true` in `docker-compose.yml` will keep the container running. Pass a flag `-d` to docker compose to turn the process into a daemon `docker-compose up --build -d`, in which case remember to run `docker-compose down` when done.

However, if there is command block in `docker-compose.yml` then when that returns it will cause the container to shut down.

In fact there is no satisfactory way of keeping the container running and in the end it is best to start a blocking process.


## Project sources and resources
https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-xix-deployment-on-docker-containers


Good article on Envars and Args
https://vsupalov.com/docker-env-vars/


Issue on Volumes vs. Bind mounts for docker.
https://github.com/docker/compose/issues/4581


## Debugging and problems
If the container does not start and there is nothing but cryptic feedback it may be worth launching straight into the container by specifying an entrypoint: `docker run -it --entrypoint /bin/sh $MY_IMAGE_NAME -s`. Of course the image has to be built first from the Dockerfile with `docker build -t $MY_IMAGE_NAME .` `$MY_IMAGE_NAME = pelican-base-dev` by default.



# Questions, problems and answers, and todos
todo Write a script that checks for a loaded pelican site content and if not run `pelican-quickstart`.

Q: How Pelican pushes. The rsync method is `rsync -avc --delete output/ host.example.com:/var/www/your-site/` but there will be prefered ways.

Q: does docker's entrypoint accept calls even if there is a blocking process running in the container?

Problem:
Getting straight at the python venv when entering the container. Possibilities:
* Create a script that is the entrypoint and then it runs the command and arguments (`exec "$@"`).
* Set the $PATH in the container directly to the `venv/bin` directory which thus will allow calling the binaries there. 
* Makefile sets a script in `/etc/profile.d/myscript.sh` which enters the venv when `/bin/sh` starts.






