#!/bin/sh

# Script which enters the python virtual environment and then starts the included HTTP sever.
echo "Entering Python venv"
source ../venv/bin/activate

#Implement pelican-quickstart if no content exists

echo "Starting Pelican HTTP server"
echo "Listening on port: ${HTTP_PORT}"
echo "Serving Site: ${SITE_DIR}"

pelican --listen --autoreload --port $HTTP_PORT
